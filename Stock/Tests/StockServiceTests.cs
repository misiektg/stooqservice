﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using Stock.Abstract;
using Stock.Models;
using Stock.Services;

namespace Stock.Tests
{
    [TestFixture]
    public class StockServiceTests
    {
        private StockService _service;
        private Mock<IStockIndexesProvider> _stockIndexesProviderMock;
        private Mock<IStockIndexesHandler> _stockIndexesHandlerMock;
        private StockIndexId[] _stockIndexIds;

        [SetUp]
        public void Setup()
        {
            _stockIndexesProviderMock = new Mock<IStockIndexesProvider>();

            _stockIndexesHandlerMock = new Mock<IStockIndexesHandler>();

            _service = new StockService(_stockIndexesProviderMock.Object, _stockIndexesHandlerMock.Object);

            _stockIndexIds = new[] {StockIndexId.MWIG40, StockIndexId.WIG};
        }

        [Test]
        public void ShouldUpdateStockIndexesCallStockIndexProvider()
        {
            _service.UpdateStockIndexes(_stockIndexIds);

            _stockIndexesProviderMock.Verify(i => i.DowlnloadStockIndexes(It.IsAny<IEnumerable<StockIndexId>>()), Times.Once);
        }

        [Test]
        public void ShouldUpdateStockIndexesCallHandler()
        {
            _service.UpdateStockIndexes(_stockIndexIds);

            _stockIndexesHandlerMock.Verify(i => i.Handle(It.IsAny<IEnumerable<StockIndex>>()));
        }
    }
}