﻿using Quartz.Spi;
using Stock.Abstract;
using Stock.Concrete;
using Stock.Services;
using StockApp.Quartz;

namespace StockApp.Container
{
    public static class IoC
    {
        public static SimpleInjector.Container Container { get; private set; }

        public static void BootStrap(SimpleInjector.Container container)
        {
            Container = container;

            IJobFactory jobFactory = new SimpleInjectorJobFactory(Container);

            Container.RegisterSingleton(jobFactory);

            Container.Register<IQuartzInitializer, QuartzInitializer>();

            container.Register<StockService>();

            container.RegisterSingleton<IRootPath,CurrentExecutablePath>();

            container.RegisterSingleton<IPersist, ToJsonFilePersitance>();

            container.Register<IStockIndexesProvider, FromStooqJsScriptProvier>();

            container.RegisterSingleton<IStockIndexesHandler, OnlyChangedStockIndexesHandler>();

            JobRegistrator jobRegistrar = new JobRegistrator(Container);

            jobRegistrar.RegisterJobs();
        }

        public static void TearDown()
        {
            Container?.Dispose();
        }
    }
}