﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using Stock.Abstract;
using Stock.Models;

namespace Stock.Concrete
{
    public class ToJsonFilePersitance : IPersist
    {
        private readonly IRootPath _rootPathProvider;

        public ToJsonFilePersitance(IRootPath rootPathProvider)
        {
            _rootPathProvider = rootPathProvider;
        }

        public void Persist(IEnumerable<StockIndex> stockIndexes)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(IEnumerable<StockIndex>));
                serializer.WriteObject(stream, stockIndexes);
                string json = Encoding.UTF8.GetString(stream.ToArray());
                File.WriteAllText(Path.Combine(_rootPathProvider.GetRootPath(), $"stoq_{DateTime.Now.ToString("yyyyMMddTHHmmss")}.txt"), json);
            }
        }
    }
}