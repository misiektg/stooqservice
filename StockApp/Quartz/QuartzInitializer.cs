﻿using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using StockApp.Jobs;

namespace StockApp.Quartz
{
    public class QuartzInitializer : IQuartzInitializer
    {
        private const string DownloadServiceTriggerKey = "DownloadService";
        private readonly IJobFactory _jobFactory;
        private IScheduler _sched;
        private readonly TriggerKey _tk;
        private IJobDetail _jobDetail;

        public QuartzInitializer(IJobFactory jobFactory)
        {
            _tk = new TriggerKey(DownloadServiceTriggerKey);
            _jobFactory = jobFactory;
        }

        public void Start(int secnodsInterval)
        {
            Common.Logging.LogManager.Adapter = new Common.Logging.Simple.ConsoleOutLoggerFactoryAdapter { Level = Common.Logging.LogLevel.Info };

            ISchedulerFactory schedFact = new StdSchedulerFactory();

            _sched = schedFact.GetScheduler();
            _sched.JobFactory = _jobFactory;
            _sched.Start();

            _jobDetail = JobBuilder.Create<DownloadStockIndexesJob>().Build();

            _sched.ScheduleJob(_jobDetail, CreateTrigger(secnodsInterval));
        }

        private ITrigger CreateTrigger(int secondsInterval)
        {
            return TriggerBuilder.Create().WithIdentity(_tk)
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(secondsInterval)
                    .RepeatForever())
                .Build();
        }

        public void ChangeInterval(int seconds)
        {
            _sched.UnscheduleJob(_tk);

            _sched.ScheduleJob(_jobDetail, CreateTrigger(seconds));
        }

        public void Stop()
        {
            _sched.Shutdown();
        }
    }
}