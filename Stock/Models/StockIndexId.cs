﻿using System.Runtime.Serialization;

namespace Stock.Models
{
    [DataContract]
    public enum StockIndexId
    {
        WIG,
        WIG20,
        WIG20FUT,
        MWIG40,
        SWIG80
    }
}