using System.Collections.Generic;
using Stock.Models;

namespace Stock.Abstract
{
    public interface IPersist
    {
        void Persist(IEnumerable<StockIndex> stockIndexes);
    }
}