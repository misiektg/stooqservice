﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using HtmlAgilityPack;
using Stock.Abstract;
using Stock.Models;

namespace Stock.Concrete
{
    public class FromStooqJsScriptProvier : IStockIndexesProvider
    {
        private readonly Dictionary<string, StockIndexId> _stockIndexesMap;

        public FromStooqJsScriptProvier()
        {
            _stockIndexesMap = new Dictionary<string, StockIndexId>()
            {
                {"mWIG40", StockIndexId.MWIG40},
                {"sWIG80", StockIndexId.SWIG80},
                {"WIG", StockIndexId.WIG},
                {"WIG20", StockIndexId.WIG20},
                {"WIG20 Fut", StockIndexId.WIG20FUT}
            };
        }

        public IEnumerable<StockIndex> DowlnloadStockIndexes(IEnumerable<StockIndexId> indicesToDownload)
        {
            string js = String.Empty;
            using (WebClient wc = new WebClient())
            {
                js = wc.DownloadString(new Uri("http://s.stooq.pl/pp/g.js"));
            }

            var parsedIndexes = Parse(js);

            return parsedIndexes.Where(i => indicesToDownload.Contains(i.Id)).ToList();
        }

        private IEnumerable<StockIndex> Parse(string js)
        {
            var strippedString = js.Replace("function pp_m_(a){if(typeof(pp_m)=='function')pp_m(a);}document.write('", string.Empty)
                 .Replace("');", string.Empty);

            var html = new HtmlDocument();

            html.LoadHtml(strippedString);

            var trs = html.DocumentNode.Descendants("tr");

            var list = new List<StockIndex>();

            foreach (var htmlNode in trs.Skip(1))
            {
                var indexName = htmlNode.ChildNodes[0].FirstChild.InnerText;

                if (!IsIndexSupported(indexName))
                {
                    continue;
                }

                var value = htmlNode.ChildNodes[1].InnerText;
                var date = htmlNode.ChildNodes[3].InnerText;

                list.Add(new StockIndex { Id = _stockIndexesMap[indexName], Date = date, Value = value, StockIndexName = indexName });
            }

            return list;
        }

        private bool IsIndexSupported(string index)
        {
            return _stockIndexesMap.ContainsKey(index);
        }
    }
}