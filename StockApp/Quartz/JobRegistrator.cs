﻿using System;
using System.Collections.Generic;
using System.Linq;
using Quartz;

namespace StockApp.Quartz
{
    public class JobRegistrator
    {
        private readonly SimpleInjector.Container _container;

        public JobRegistrator(SimpleInjector.Container container)
        {
            _container = container;
        }

        private static IEnumerable<Type> GetJobTypes()
        {
            return AppDomain.CurrentDomain.GetAssemblies().ToList()
                .SelectMany(s => s.GetTypes())
                .Where(p => typeof(IJob).IsAssignableFrom(p) && !p.IsInterface);
        }

        public void RegisterJobs()
        {
            var jobTypes = GetJobTypes();

            foreach (Type jobType in jobTypes)
            {
                _container.Register(jobType);
            }
        }
    }
}