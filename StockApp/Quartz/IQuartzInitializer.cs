﻿namespace StockApp.Quartz
{
    public interface IQuartzInitializer
    {
        void Start(int secondsInterval);

        void Stop();

        void ChangeInterval(int seconds);
    }
}