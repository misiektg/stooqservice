﻿using System;
using System.Collections.Generic;
using Quartz;
using Stock.Models;
using Stock.Services;

namespace StockApp.Jobs
{
    public class DownloadStockIndexesJob : IJob
    {
        private readonly StockService _stockService;

        private readonly IEnumerable<StockIndexId> _stockIndexesToDownload;

        public DownloadStockIndexesJob(StockService stockService)
        {
            _stockService = stockService;
            _stockIndexesToDownload = new[] { StockIndexId.WIG, StockIndexId.MWIG40, StockIndexId.SWIG80, StockIndexId.WIG20, StockIndexId.WIG20FUT };
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                _stockService.UpdateStockIndexes(_stockIndexesToDownload);
            }
            catch (Exception ex)
            {
                throw new JobExecutionException(ex);
            }
        }
    }
}