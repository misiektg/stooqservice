namespace Stock.Abstract
{
    public interface IRootPath
    {
        string GetRootPath();
    }
}