using System;
using System.Collections.Generic;
using System.Linq;
using Stock.Abstract;
using Stock.Common;
using Stock.Models;

namespace Stock.Concrete
{
    public class OnlyChangedStockIndexesHandler : IStockIndexesHandler
    {
        private readonly IPersist _persist;

        private IEnumerable<StockIndex> _stockExchangeIndexes;

        private readonly object _lockObject = new object();

        public OnlyChangedStockIndexesHandler(IPersist persist)
        {
            _persist = persist;
        }

        public void Handle(IEnumerable<StockIndex> stockExchangeIndexes)
        {
            if (stockExchangeIndexes == null)
            {
                throw new ArgumentNullException();
            }

            stockExchangeIndexes = stockExchangeIndexes.ToArray();

            lock (_lockObject) // Simple lock. Sorry for that.
            {
                if (_stockExchangeIndexes == null)
                {
                    _persist.Persist(stockExchangeIndexes);

                    _stockExchangeIndexes = stockExchangeIndexes;

                    return;
                }

                var changedStockIndexes = stockExchangeIndexes.Except(_stockExchangeIndexes, new StockIndexEqualityComparer()).ToArray();

                _stockExchangeIndexes = stockExchangeIndexes;

                if (!changedStockIndexes.Any())
                {
                    return;
                }

                _persist.Persist(changedStockIndexes);
            }
        }
    }
}