using System.Collections.Generic;
using Stock.Abstract;
using Stock.Models;

namespace Stock.Services
{
    public class StockService
    {
        private readonly IStockIndexesProvider _stockIndexesProvider;

        private readonly IStockIndexesHandler _stockIndexesHandler;

        public StockService(IStockIndexesProvider stockIndexesProvider, IStockIndexesHandler stockIndexesHandler)
        {
            _stockIndexesProvider = stockIndexesProvider;
            _stockIndexesHandler = stockIndexesHandler;
        }

        public void UpdateStockIndexes(IEnumerable<StockIndexId> stockIndexIds)
        {
            var downloadedIndexes = _stockIndexesProvider.DowlnloadStockIndexes(stockIndexIds);

            _stockIndexesHandler.Handle(downloadedIndexes);
        }
    }
}