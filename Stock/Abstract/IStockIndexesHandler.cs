using System.Collections.Generic;
using Stock.Models;

namespace Stock.Abstract
{
    public interface IStockIndexesHandler
    {
        void Handle(IEnumerable<StockIndex> stockExchangeIndexes);
    }
}