﻿using System;
using System.Runtime.Serialization;

namespace Stock.Models
{
    [DataContract]
    public class StockIndex
    {
        [DataMember(Name = "id")]
        public StockIndexId Id { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "date")]
        public string Date { get; set; }

        [DataMember(Name = "indexName")]
        public string StockIndexName { get; set; }
    }
}