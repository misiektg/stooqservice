using System.Collections.Generic;
using Stock.Models;

namespace Stock.Common
{
    public class StockIndexEqualityComparer : EqualityComparer<StockIndex>
    {
        public override bool Equals(StockIndex x, StockIndex y)
        {
            return (x != null && y != null || x == null && y == null) && (x == y || x.Id == y.Id && x.Value == y.Value);
        }

        public override int GetHashCode(StockIndex obj)
        {
            if (obj == null)
            {
                return 0;
            }

            unchecked
            {
                var hashCode = (int)obj.Id;
                hashCode = (hashCode * 397) ^ obj.Value.GetHashCode();
                return hashCode;
            }
        }
    }
}