using System.Collections.Generic;
using Stock.Models;

namespace Stock.Abstract
{
    public interface IStockIndexesProvider
    {
        IEnumerable<StockIndex> DowlnloadStockIndexes(IEnumerable<StockIndexId> indicesToDownload);
    }
}