﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using Stock.Abstract;
using Stock.Common;
using Stock.Concrete;
using Stock.Models;

namespace Stock.Tests
{
    [TestFixture]
    public class OnlyChangedStockIndexesHandlerTests
    {
        private Mock<IPersist> _persistanceMock;
        private IStockIndexesHandler _handler;

        [SetUp]
        public void Setup()
        {
            _persistanceMock = new Mock<IPersist>();

            _handler = new OnlyChangedStockIndexesHandler(_persistanceMock.Object);
        }

        [Test]
        public void ShouldPersistIndexesWithFirstInvoke()
        {
            var initialIndexes = new[]
            {
                new StockIndex() {Id = StockIndexId.MWIG40, Value = "1000"},
                new StockIndex() {Id = StockIndexId.WIG, Value = "2000"}

            };

            _handler.Handle(initialIndexes);

            _persistanceMock.Verify(i => i.Persist(initialIndexes), Times.Once);
        }

        [Test]
        public void ShouldNotPersistStockIndexesIfNotChanged()
        {
            var initialIndexes = new[]
           {
                new StockIndex() {Id = StockIndexId.MWIG40, Value = "1000"},
                new StockIndex() {Id = StockIndexId.WIG, Value = "2000"}
            };

            _handler.Handle(initialIndexes);

            var updatedIndexes = new[]
            {
                new StockIndex() {Id = StockIndexId.MWIG40, Value = "1000"},
                new StockIndex() {Id = StockIndexId.WIG, Value = "2000"}
            };

            _handler.Handle(updatedIndexes);

            _persistanceMock.Verify(i => i.Persist(updatedIndexes), Times.Never());
        }

        [Test]
        public void ShouldPersistOnlyChangedStockIndexes()
        {
            var initialIndexes = new[]
            {
                new StockIndex() {Id = StockIndexId.MWIG40, Value = "1000"},
                new StockIndex() {Id = StockIndexId.WIG, Value = "2000"}
            };

            _handler.Handle(initialIndexes);

            var changedIndex = new StockIndex() { Id = StockIndexId.MWIG40, Value = "2000" };

            var updatedIndexes = new[]
            {
                changedIndex,
                new StockIndex() {Id = StockIndexId.WIG, Value = "2000"}

            };

            _handler.Handle(updatedIndexes);

            _persistanceMock.Verify(i => i.Persist(It.Is<IEnumerable<StockIndex>>(p => p.SequenceEqual(new[] { changedIndex }, new StockIndexEqualityComparer()))), Times.Once);
        }

        [Test]
        public void ShouldThrowArgumentNullExceptionIfNullPassed()
        {
            Assert.Throws<ArgumentNullException>(() => _handler.Handle(null));
        }

        [Test]
        public void ShouldNotThrowArgumentNullExceptionIfNotNullPassed()
        {
            Assert.DoesNotThrow(() => _handler.Handle(Enumerable.Empty<StockIndex>()));
        }
    }
}