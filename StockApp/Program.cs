﻿using System;
using System.IO;
using Stock.Abstract;
using StockApp.Container;
using StockApp.Quartz;

namespace StockApp
{
    class Program
    {
        private static IQuartzInitializer _initializer;

        static void Main(string[] args)
        {
            var container = new SimpleInjector.Container();

            IoC.BootStrap(container);

            _initializer = IoC.Container.GetInstance<IQuartzInitializer>();

            Console.WriteLine($"Stock files path:{container.GetInstance<IRootPath>().GetRootPath()}\n");

            while (true)
            {
                Console.WriteLine("\nEnter stock download time interval in seconds and press Enter or just press Enter to use default value(60s).\n\nEnter q and press enter for exit.\n");

                try
                {
                    var input = Console.ReadLine();

                    int interval;

                    if (input.ToLower() == "q")
                    {
                        return;
                    }

                    var useDefault = string.IsNullOrEmpty(input);

                    if (int.TryParse(input, out interval) || useDefault)
                    {
                        useDefault = interval <= 0;

                        _initializer.Start(useDefault ? 60 : interval);

                        if (useDefault)
                        {
                            Console.WriteLine("\nUsed default time interval(60s)");
                        }

                        Console.WriteLine("\nService started");

                        break;
                    }

                    Console.WriteLine("\nWrong inverval format");
                }
                catch (IOException)
                {
                    Console.WriteLine("\nWrong inverval format");
                }
                catch (OutOfMemoryException)
                {
                    Console.WriteLine("\nOut of memory exception. Press key to exit program");

                    Console.ReadKey();
                    return;
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("\nWrong inverval format");
                }
            }

            Console.WriteLine("\nPress any to exit program\n");

            Console.ReadKey();

            _initializer.Stop();
        }
    }
}

