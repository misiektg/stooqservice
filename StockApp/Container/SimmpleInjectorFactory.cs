﻿using Quartz;
using Quartz.Spi;

namespace StockApp.Container
{
    public class SimpleInjectorJobFactory : IJobFactory
    {
        private readonly SimpleInjector.Container _container;

        public SimpleInjectorJobFactory(SimpleInjector.Container container)
        {
            _container = container;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return (IJob)_container.GetInstance(bundle.JobDetail.JobType);
        }

        public void ReturnJob(IJob job)
        {
        }
    }
}