using System;
using Stock.Abstract;

namespace StockApp
{
    public class CurrentExecutablePath : IRootPath
    {
        public string GetRootPath()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }
    }
}